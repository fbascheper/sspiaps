# This file is part of 'NTLM Authorization Proxy Server'
# Copyright 2001 Dmitry A. Rozmanov <dima@xenon.spb.ru>
#
# NTLM APS is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# NTLM APS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the sofware; see the file COPYING. If not, write to the
# Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
#

import httplib
import string
import base64

import ntlm_procs # TODO: remove
import utils # TODO: remove


#---------------------------------------------------------------------
class NativeAuth:

    #-----------------------------------------------------------------------
    def __init__(self, config, logger):
        self.config = config
        self.logger = logger
        self.auth_info = None

        # if options.user or options.domain or options.password:
        #     self.auth_info = options.user, options.domain, options.password

        try:
            import sspi

        except ImportError as error:
            handle_error(error)

        self.ca = sspi.ClientAuth("NTLM", auth_info=self.auth_info)
        self.auth_scheme = self.ca.pkg_info['Name']
        self.data = None

        self.logger.log('*** Auth scheme = ' + self.auth_scheme +'\n')

    def step1Auth(self):
        err, out_buf = self.ca.authorize(self.data)
        self.data = out_buf[0].Buffer

        # Encode it as base64 as required by HTTP
        auth = base64.encodestring(self.data).replace("\012", "")

        self.logger.log('*** Authorization = ' + auth +'\n')

        return auth

    #---------------------------------------------------------------------
    def parse_step1AuthResp(self, msg2):
        ""
        msg2 = base64.decodestring(msg2)
        # protocol = msg2[0:7]
        # msg_type = msg2[7:9]
        nonce = msg2[24:32]

        return nonce

    def step2Auth(self, msg2, environment_dict):

        self.logger.log('*** step2Auth - msg2 = ' + msg2 +'\n')

        ed = environment_dict

        #domain_rec = record(ed['DOMAIN'])
        #user_rec = record(ed['USER'])
        #host_rec = record(ed['HOST'])

        self.data = base64.decodestring(msg2)

        err, out_buf = self.ca.authorize(self.data)
        self.data = out_buf[0].Buffer

        # Encode it as base64 as required by HTTP
        auth = base64.encodestring(self.data).replace("\012", "")

        self.logger.log('*** step2Auth - Authorization = ' + auth +'\n')

        return auth


#---------------------------------------------------------------------

def open_url(host, url):

    try:
        import sspi
    except ImportError as error:
        handle_error(error)

    options = None # set to optparse options object
    h = httplib.HTTPConnection(host)
#    h.set_debuglevel(9)
    h.putrequest('GET', url)
    h.endheaders()
    resp = h.getresponse()
    print "Initial response is", resp.status, resp.reason
    body = resp.read()
    if resp.status == 302: # object moved
        url = "/" + resp.msg["location"]
        resp.close()
        h.putrequest('GET', url)
        h.endheaders()
        resp = h.getresponse()
        print "After redirect response is", resp.status, resp.reason
    if options.show_headers:
        print "Initial response headers:"
        for name, val in resp.msg.items():
            print " %s: %s" % (name, val)
    if options.show_body:
        print body
    if resp.status == 401:
        # 401: Unauthorized - here is where the real work starts
        auth_info = None
        if options.user or options.domain or options.password:
            auth_info = options.user, options.domain, options.password
        ca = sspi.ClientAuth("NTLM", auth_info=auth_info)
        auth_scheme = ca.pkg_info['Name']
        data = None
        while 1:
            err, out_buf = ca.authorize(data)
            data = out_buf[0].Buffer
            # Encode it as base64 as required by HTTP
            auth = base64.encodestring(data).replace("\012", "")
            h.putrequest('GET', url)
            h.putheader('Authorization', auth_scheme + ' ' + auth)
            h.putheader('Content-Length', '0')
            h.endheaders()
            resp = h.getresponse()
            if options.show_headers:
                print "Token dance headers:"
                for name, val in resp.msg.items():
                    print " %s: %s" % (name, val)

            if err==0:
                break
            else:
                if resp.status != 401:
                    print "Eeek - got response", resp.status
                    cl = resp.msg.get("content-length")
                    if cl:
                        print repr(resp.read(int(cl)))
                    else:
                        print "no content!"

                assert resp.status == 401, resp.status

            assert not resp.will_close, "NTLM is per-connection - must not close"
            schemes = [s.strip() for s in resp.msg.get("WWW-Authenticate", "").split(",")]
            for scheme in schemes:
                if scheme.startswith(auth_scheme):
                    data = base64.decodestring(scheme[len(auth_scheme)+1:])
                    break
            else:
                print "Could not find scheme '%s' in schemes %r" % (auth_scheme, schemes)
                break
        
            resp.read()
    print "Final response status is", resp.status, resp.reason
    if resp.status == 200:
        # Worked!
        # Check we can read it again without re-authenticating.
        if resp.will_close:
            print "EEEK - response will close, but NTLM is per connection - it must stay open"
        body = resp.read()
        if options.show_body:
            print "Final response body:"
            print body
        h.putrequest('GET', url)
        h.endheaders()
        resp = h.getresponse()
        print "Second fetch response is", resp.status, resp.reason
        if options.show_headers:
            print "Second response headers:"
            for name, val in resp.msg.items():
                print " %s: %s" % (name, val)
        
        resp.read(int(resp.msg.get("content-length", 0)))
    elif resp.status == 500:
        print "Error text"
        print resp.read()
    else:
        if options.show_body:
            cl = resp.msg.get("content-length")
            print resp.read(int(cl))



#---------------------------------------------------------------------
def handle_error(error):
    print "You don't have module {0} installed; ".format(error.message[16:])
    raise(RuntimeError("PyWin32 (sspi) missing"))
