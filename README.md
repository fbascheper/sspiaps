SSPI Authenticating proxy


This project is based on the NTLMAPS project, but supports the use of native SSPI on Windows
using the Pywin32 library. To enable this, you must set 
	USE_NATIVE_WIN32_SSPI:1
in the server.cfg file

Then you don't need to add your username, password or domain in the file. They are retrieved
using the windows API's supported by pywin32.



Authenticated GIT SVN

GIT SVN uses a special version of the Subversion client, which can go through a proxy.
On Windows this is usually configured in "%APPDATA%\Subversion". 

However, for GIT SVN this should be 
 
write %HOME%\.subversion\servers

where the HOME environment variable is already defined for the git win32 client.

 
